#include<iostream>
using namespace std;
class shape{
    public:
    virtual void name()=0;
};
class Pentagon : public shape{
    public:
    void name(){
        cout<<"Pentagon"<<endl;
    }
};
class Trapezium : public shape{
    public:
    void name(){
        cout<<"Trapezium"<<endl;
    }
};
int main(){
    shape *b;
    Pentagon obj;
    b = &obj;
    b->name();
    Trapezium o;
    b = &o;
    b->name();
    return 0;
}