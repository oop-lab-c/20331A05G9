class Main
{
    public void student()
    {
        String name = "AhmadAlisha";
        double sempercentage = 76.7;
        int rollno = 45;
        String collegename = "MVGR";
        int collegecode = 33;
        System.out.println("Name : "+name);
        System.out.println("SemPercentage : "+sempercentage);
        System.out.println("RollNo : "+rollno);
        System.out.println("CollegeName : "+collegename);
        System.out.println("CollegeCode : "+collegecode);
    }
    protected void finalize()
    {
        System.out.println("Finalize method Invoked");
    }
    public static void main (String[] args) {
        Main obj = new Main();
        obj.student();
        obj.finalize();
    }
}