class A
{
    public void m1()
    {
        System.out.println("Hi....");
    }
    protected void m2()
    {
        System.out.println("Java....");
    }
    private void m3()
    {
        System.out.println("OOPS....");
    }
}
class B
{
    public static void main(String[] args)
    {
        A a = new A();
        a.m1();
        a.m2();
        a.m3();
    }
}