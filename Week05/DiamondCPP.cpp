#include<iostream>
using namespace std;
class GrandParent
{
    public:
    virtual void displayAge()=0;
};
class Parent1 : public GrandParent
{
    public:
    int par1Age=45;
};
class Parent2 : public GrandParent
{
    public:
    int par2Age=35;
};
class child : public Parent1,Parent2
{
    public:
    int age;
    void setAge()
    {
        age =65;
    }
    void displayAge()
    {
        cout<<age<<endl;
        cout<<par1Age<<endl;
        cout<<par2Age<<endl;
    }
};
int main()
{
    child obj;
    obj.setAge();
    obj.displayAge();
}