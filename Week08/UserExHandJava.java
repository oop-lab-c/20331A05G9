import java.lang.Exception;
import java.util.Scanner;
class AgeError extends Exception{
    AgeError(){
        super("Age is not eligible to vote");
    }
}
class CExcep{
    public static void main(String[] args) {
        System.out.println("Enter the age = ");
        Scanner sc  =  new Scanner(System.in);
        int  a = sc.nextInt();
        try{
            if(a < 18){
                throw new AgeError();
            }
        }
        catch(AgeError e){
            System.out.println(e.toString());
            e.printStackTrace();
        }

    }
}
