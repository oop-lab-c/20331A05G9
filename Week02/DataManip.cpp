#include<iostream>
#include<iomanip>
using namespace std;
int main(int argc, char const *argv[])
{
    string a = "Ashik";
    string b = "Ahamad";
    cout<<"First name = "<<a;
    cout<<"\tSecond name = "<<b;  
    cout<<"\n\nDemonstration of endl = "<<endl; //skip to next line
    cout<<"First name = "<<a<<endl;
    cout<<"Second name = "<<b<<endl;
    cout<<"\nDemonstration of setw(int) ="<<endl;  //fill given input from right to left and remaining spaces
    cout<<setw(10)<<a<<endl;
    cout<<"\nDemonstration of setfill(int)"<<endl;  // the spaces wiil be filled by given char
    cout<<setfill('#')<<setw(10)<<a<<endl;
    double c = 72.45892; 
    cout<<"\nDemonstration of setprecision(int)"<<endl; // display give no. up to that value
    cout<<setprecision(4)<<c<<endl;
    cout<<"\nDemonstration of ends"<<endl;//add next line in the present line
    cout<<a<<ends;
    cout<<b<<endl;
    cout<<"\nDemonstration of ws"<<endl; //takes leading spaces in input
    cout<<"Enter your name by adding spaces before it = "<<endl;
    string d;               
    cin>>ws>>d;
    cout<<"Entered names is ="<<d<<endl;
    cout<<"\nDemonstration of flush"<<endl; //flush will flush the next line same as ends
    cout<<a<<flush;
    cout<<b<<endl;
    return 0;
} 
