import java.io.*;
class Main extends Thread
{
    public void run()
    {
        System.out.println("Hi..This message was executed by extending Thread class to Main");
    }
    public static void main(String[] args)
    {
        Main obj = new Main();
        obj.start();
    }
}